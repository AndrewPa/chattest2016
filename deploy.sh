#!/usr/bin/env bash

# In server app directory
echo Installing server side node modules
cd ./node
npm install

# In client app directory
echo Installing client node modules
cd ../client
npm install

echo Running gulp deploy
gulp deploy

echo Copying assets to build directory
cp -r sounds/ build/deps/sounds

echo Copying fonts to build directory
mkdir -p build/deps/css/font-awesome/fonts
cp -r src/scss/font-awesome/fonts build/deps/css/font-awesome/