'use strict';

let setAppGlobals = require('./topLevelFunctions/setAppGlobals').default;

// Everything that establishes the structure of the global namespace
// contained in the source for setAppGlobals, with explanations
setAppGlobals();

let setUpMainSocket = require('./topLevelFunctions/setUpMainSocket').default;

// Main entry point for server and socket initialization and configuration
setUpMainSocket();

// Declare in this manner that the global namespace will be used for
// any reason in any file, module or function
// let chattestServer = global.chattestServer;




