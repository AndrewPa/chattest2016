'use strict';

let http = require('./deps/configureHttp').default;
let setSocketListeners = require('./deps/setSocketListeners').default;
let setSocketEmitters = require('./deps/setSocketEmitters').default;
let sessionMiddleware = require('./deps/sessionMiddleware').default;
let OnlineUserStore = require('./deps/OnlineUserStore').default;

let io = require('socket.io')(http);

exports.default = function() {

  io.use(function(socket, next) {
    sessionMiddleware(socket.request, {}, next);
  })
  .on('connection', function(socket) {

    /* TODO: Encapsulate init/destroy stuff elsewhere */
    let chattestServer = global.chattestServer;
    let socketInstanceId;
    let userId;

    try {
      userId = socket.request.session.passport.user;
      socketInstanceId = socket.client.conn.id;
    }
    catch (e) {
      console.error(`Error during connect: ${e}`);
      return false;
    }

    //console.log(userId + " has connected, socket id: " + socket.id);

    let socketForUser = global.chattestServer.getSocketForUser(userId, socket);
    if (socketForUser) {
      socketForUser.emit("userAlreadyHasConnection");
      socketForUser.disconnect();
    }
    global.chattestServer.setSocketForUser(userId, socket);

    chattestServer.createChannelForSocketId(socketInstanceId);

    setSocketListeners(io, socket);
    setSocketEmitters(io, socket);

    OnlineUserStore.addUserToOnlineList(userId);
    // Put this in the user store class, wrap it in function
    chattestServer.getChannelForSocketId(socketInstanceId).emit(
      'onlineUserListUpdated',
      OnlineUserStore.getOnlineUserList()
    );

    socket.on('disconnect', function() {

      try {
        userId = socket.request.session.passport.user;
      }
      catch (e) {
        console.error(`Error during disconnect: ${e}`);
        return false;
      }

      //console.log(userId + " has disconnected, socket id: " + socket.id);

      OnlineUserStore.removeUserFromOnlineList(userId);
      chattestServer.getChannelForSocketId(socketInstanceId).emit(
        'onlineUserListUpdated',
        OnlineUserStore.getOnlineUserList()
      );

      chattestServer.destroyChannelForSocketId(socketInstanceId);

    });

  });

  // This should be on the main entry point -- revise
  http.startListening();

};