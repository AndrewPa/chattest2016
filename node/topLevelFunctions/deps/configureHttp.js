'use strict';

let app = require('./setUpExpressApp').default;
let http = require('http').Server(app);

let chattestServer = global.chattestServer;

http.startListening = function() {
  http.listen(chattestServer.constants.listeningPort, function(){
    console.log(`Listening on port ${chattestServer.constants.listeningPort}`);
  });
};

exports.default = http;