'use strict';

let mongoose = require('mongoose');

let messageSchema = new mongoose.Schema({
  sentBy: String,
  body: String,
  date: { type: Date, default: Date.now }
});

exports.default = mongoose.model('Message', messageSchema);