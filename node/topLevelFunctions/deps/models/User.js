'use strict';

let mongoose = require('mongoose');

let preferenceSchema = new mongoose.Schema({
  preference: String,
  value: Number
});


let userSchema = new mongoose.Schema({
  userId: String,
  password: String,
  preferences: [preferenceSchema],
  mutedUsers: Array
});

exports.default = mongoose.model('User', userSchema);