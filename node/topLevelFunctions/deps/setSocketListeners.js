'use strict';

let socketEventHandlers = require('./socketHandlers').default;

exports.default = function setSocketListeners(io, socket) {

  Object.keys(socketEventHandlers).forEach(function(key) {

    let handler = socketEventHandlers[key];

    if (typeof handler !== "function") {
      throw Error("A non-function property was encountered in an instance of SocketEventHandlers");
    }

    // N.B. A handler's name will be the same as the event that fired it,
    // and all events names should be in camelCase
    socket.on(key, function(...args) {
      handler(io, socket, ...args);
    });

  });

};