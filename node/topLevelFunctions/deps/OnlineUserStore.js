'use strict';

// Enforced singleton class
exports.default = new class OnlineUserStore {

  constructor() {

    let onlineUsers = {};

    this.addUserToOnlineList = function (userId) {
      onlineUsers[userId] = true;
    };

    this.removeUserFromOnlineList = function(userId) {
      delete onlineUsers[userId];
    };

    this.getOnlineUserList = function() {
      return Object.keys(onlineUsers);
    };

    this.isUserOnline = function(userId) {
      return onlineUsers[userId];
    };

  }

};