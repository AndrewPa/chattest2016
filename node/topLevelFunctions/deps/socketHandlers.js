// All handlers for socket events go here
'use strict';

let OnlineUserStore = require('./OnlineUserStore').default;

let dbOps = require('./dbOps').default;

exports.default = {

  /*
     Messages are pushed to the client immediately upon reception.
     If a user receives a message from another that they have muted,
     the client receives it but does not display it.

     The initial chat message pull, however, filters messages
     based on the user's muted user list before sending them.
     For this reason, they are handled through different
     events.
  */

  chatMsg: function(io, socket, msg) {

    /* TODO: More DRY */
    let chattestServer = global.chattestServer;
    let socketInstanceId = socket.client.conn.id;
    let channelForSocket = chattestServer.getChannelForSocketId(socketInstanceId);

    msg.sentBy = socket.request.session.passport.user;

    dbOps.addMsgToDB(msg).then(function(messageData) {
      channelForSocket.emit('gotNewRealTimeMessage', messageData);
    }).catch(function(e) {
      console.error(e);
    });

  },

  clientGetNewMsgs: function(io, socket) {

    let chattestServer = global.chattestServer;
    let socketInstanceId = socket.client.conn.id;
    let channelForSocket = chattestServer.getChannelForSocketId(socketInstanceId);
    let userId = socket.request.session.passport.user;

    dbOps.getMessagesFromDB(userId).then(function(messageData) {
      channelForSocket.emit('gotInitMessageList', messageData);
    }).catch(function(e) {
      console.error(e);
    });

  },

  clientGetOnlineUsers: function(io, socket) {

    let chattestServer = global.chattestServer;
    let socketInstanceId = socket.client.conn.id;
    let channelForSocket = chattestServer.getChannelForSocketId(socketInstanceId);

    let onlineUserList = OnlineUserStore.getOnlineUserList();
    channelForSocket.emit('gotOnlineUserList', onlineUserList);

  },

  clientGetUserData: function(io, socket) {

    let chattestServer = global.chattestServer;
    let socketInstanceId = socket.client.conn.id;
    let channelForSocket = chattestServer.getChannelForSocketId(socketInstanceId);

    let userId = socket.request.session.passport.user;

    dbOps.getUserData(userId).then(function(userData) {
      channelForSocket.emit('gotUserData', userData);
    }).catch(function(e) {
      console.error(e);
    });

  },

  savePreferences: function(io, socket, prefArr) {
    let userId = socket.request.session.passport.user;
    dbOps.saveUserPrefsToDb(userId, prefArr).catch(function(e) {
      console.error(e);
    });
  },

  toggleMutedUser: function(io, socket, mutedUserId) {

    let chattestServer = global.chattestServer;
    let socketInstanceId = socket.client.conn.id;
    let channelForSocket = chattestServer.getChannelForSocketId(socketInstanceId);

    let userId = socket.request.session.passport.user;

    dbOps.toggleMutedUserForUser(userId, mutedUserId).then(function() {
      channelForSocket.emit('doneToggleMuteUser', mutedUserId);
    }).catch(function(e) {
      console.error(e);
    });

  },

  error: function() {
    console.error(arguments[2]);
  }

};