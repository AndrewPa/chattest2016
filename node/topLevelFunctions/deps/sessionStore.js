'use strict';

let session = require('express-session');
let MongoStore = require('connect-mongo')(session);

exports.default = new MongoStore({ url: 'mongodb://localhost/chattest' });