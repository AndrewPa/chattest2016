'use strict';

let socketEmitters = require('./socketEmitters').default;

exports.default = function setSocketEmitters(io, socket) {

  let chattestServer = global.chattestServer;
  let socketInstanceId = socket.client.conn.id;
  let channelForSocket = chattestServer.getChannelForSocketId(socketInstanceId);

  (function setRealTimeMessagePusher() {

    channelForSocket.on('gotNewRealTimeMessage', function(messageData) {
      socketEmitters.pushRealTimeMessageToAllClients(io, socket, messageData);
    });

  })();

  (function setInitMessagePusher() {

    channelForSocket.on('gotInitMessageList', function(messageList) {
      socketEmitters.pushInitMsgListToClient(io, socket, messageList);
    });

  })();

  (function setOnlineUserUpdater() {

    channelForSocket.on('onlineUserListUpdated', function(onlineUserList) {
      socketEmitters.pushOnlineUserListToClient(io, socket, onlineUserList);
    });

  })();

  (function setOnlineUserPusher() {

    channelForSocket.on('gotOnlineUserList', function(onlineUserList) {
      socketEmitters.pushOnlineUserListToClient(io, socket, onlineUserList);
    });

  })();

  (function setUserDataPusher() {

    channelForSocket.on('gotUserData', function(userData) {
      socketEmitters.pushUserDataToClient(io, socket, userData);
    });

  })();

  (function setUserDataPusher() {

    channelForSocket.on('doneToggleMuteUser', function(mutedUserId) {
      socketEmitters.pushToggledUserStatusToClientfunction(io, socket, mutedUserId);
    });

  })();

};