'use strict';

let passport = require('passport');
let path = require('path');

let dbOps = require('./dbOps').default;
let OnlineUserStore = require('./OnlineUserStore').default;
let hbsHelpers = require('./hbsHelpers').default;

exports.indexRoute = function(req, res, next) {

  if(req.isAuthenticated()) {
    res.redirect('/main');
  }
  else {
    res.render('index', {});
  }

};

exports.doesNotExist = function(req, res, next) {
  res.render('error', {});
};

exports.disconnectedRoute = function(req, res, next) {
  res.render('disconnected', {});
};

let signInPost = exports.signInPost = function(req, res, next) {

  passport.authenticate('local', function(err, user, info) {

    if(err) {
      return res.status(400).json({"error": err});
    }

    if(!user) {
      return res.status(400).json({"error": `That username/password combination is invalid.`});
    }

    return req.logIn(user, function(err) {
      if(err) {
        return res.status(400).json({"error": err});
      } else {
        return res.status(200).json({"error": null});
      }
    });

  })(req, res, next);

};

let basicValidateUserDataSubmission = function(userData) {

  return (typeof userData !== "undefined") &&
         (typeof userData.username === "string") &&
         (typeof userData.password === "string")

};

exports.signUpPost = function(req, res, next) {

  var userData = req.body;

  if (!basicValidateUserDataSubmission(userData)) {
    return res.status(400).json({"error": `Username or password is invalid.`});
  }

  userData.username = userData.username.trim();

  // This logic belongs elsewhere

  /* Remove magic numbers to constants */
  if (userData.username.length < 4 || userData.username.length > 16) {
    return res.status(400).json({
      "error": `Username '${userData.username}' needs to be between ${4} and ${16} characters in length.`
    });
  }

  if (userData.password.length < 6 || userData.password.length > 30) {
    return res.status(400).json({
      "error": `Password needs to be between ${6} and ${30} characters in length.`
    });
  }

  if (userData.password_repeat !== userData.password) {
    return res.status(400).json({
      "error": `The passwords you entered do not match. Please try again.`
    });
  }

  return dbOps.findUserById(userData.username).then(function(user) {
    
    if(user) {
      return res.status(400).json({
        "error": `Username '${user.username}' already exists.`
      });
    } else {
      dbOps.registerUser({
        username: userData.username,
        password: userData.password
      }).then(function() {
        signInPost(req, res, next);
      }).catch(function(e) {
        console.error(e);
      });
    }
  }).catch(function(e) {
    console.error(e);
  });

};

exports.signUpRoute = function(req, res, next) {
  res.redirect('/');
};

exports.signOutRoute = function(req, res, next) {
  if (req.isAuthenticated()) { req.logout(); }
  res.redirect('/');
};

exports.signInRoute = function(req, res, next) {
  res.redirect('/');
};

exports.mainRoute = function(req, res, next) {
  if(!req.isAuthenticated()) {
    res.redirect('/');
  } else {
    res.render('main', {
      helpers: {
        raw: hbsHelpers.raw
      }
    });
  }
};