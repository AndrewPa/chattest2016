'use strict';

let expressSession = require('express-session');
let sessionStore = require('./sessionStore').default;

exports.default = expressSession({
  name: "chattestSessionData",
  secret: "ahighlysecretthingythatnobodyelsecaneverpossiblyknowexclamationpoint",
  store: sessionStore
});