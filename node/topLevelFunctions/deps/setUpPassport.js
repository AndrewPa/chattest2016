'use strict';

let bcrypt = require('bcryptjs');
let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;

let dbOps = require('./dbOps').default;

passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  function(username, password, done) {

  dbOps.findUserById(username).then(function(user) {

    if(user === null) {
      return done(null, false, {message: 'Invalid username or password'});
    } else {
      if(!bcrypt.compareSync(password, user.password)) {
        return done(null, false, {message: 'Invalid username or password'});
      } else {
        return done(null, user);
      }
    }

  }).catch(function(e) {
    console.error(e);
  });

}));

passport.serializeUser(function(user, done) {
  done(null, user.userId);
});

passport.deserializeUser(function(userId, done) {
  new dbOps.findUserById(userId).then(function(user) {
    done(null, user);
  }).catch(function(e) {
    console.error(e);
  });
});

exports.default = passport;