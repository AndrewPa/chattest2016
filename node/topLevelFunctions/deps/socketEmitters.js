// All functions that emit to the socket are here

'use strict';

exports.default = {

  /* Emit to all clients */

  pushRealTimeMessageToAllClients: function(io, socket, message) {
    io.emit('newChatData', message);
  },

  pushOnlineUserListToClient: function(io, socket, onlineUserList) {
    io.emit('newOnlineUserList', onlineUserList);
  },

  /* Emit to requesting client only */

  pushInitMsgListToClient: function(io, socket, msgList) {
    socket.emit('newChatData', msgList);
  },

  pushUserDataToClient: function(io, socket, userData) {
    socket.emit('newUserData', userData);
  },

  pushToggledUserStatusToClientfunction(io, socket, mutedUserId) {
    socket.emit('userMuteToggled', mutedUserId)
  }

};