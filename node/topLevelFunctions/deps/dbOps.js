'use strict';

let bcrypt = require('bcryptjs');
let mongoose = require('mongoose');
let validator = require('validator');

let User = require('./models/User').default;
let Message = require('./models/Message').default;

mongoose.connect("mongodb://localhost/chattest");

exports.default = {

  addMsgToDB: function(msgData) {

    let cleanBody = validator.escape(msgData.body);

    return new Message({
      sentBy: msgData.sentBy,
      body: cleanBody
    }).save();

  },

  getMessagesFromDB: function(userId) {

    let chattestServer = global.chattestServer;
    return new Promise(function(resolve, reject) {

      User.findOne({userId: userId}).exec().then(function(user) {

        return Message.find({
          sentBy: {
            $nin: user.mutedUsers
          }
        },
        null,
        {
          sort : { date: -1 },
          limit: chattestServer.constants.msgGetLimit
        }).exec();

      }).then(function(messages) {
        resolve(messages);
      }).catch(function(e) {
        console.error(e);
        reject(e);
      });

    })
    
  },

  findUserById: function(userId) {
    return User.findOne({userId: userId}).exec();
  },

  registerUser: function(userData) {

    let username = userData.username;
    let password = userData.password;

    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(password, salt);

    return new User({
      userId: username,
      password: hash
    }).save();

  },

  getUserData: function(userId) {
    return User.findOne({userId: userId}).exec();
  },

  saveUserPrefsToDb: function(userId, prefArr) {

    return new Promise(function(resolve, reject) {

      User.findOne({
        userId: userId
      }).exec().then(function(user) {
        user.preferences = prefArr;
        return user.save();
      }).then(function(updatedUser) {
        resolve(updatedUser);
      }).catch(function (e) {
        console.error(e);
        reject(e);
      });

    });

  },

  toggleMutedUserForUser: function(userId, mutedUserId) {

    return new Promise(function(resolve, reject) {

      User.findOne({
        userId: userId
      }).exec().then(function(user) {

        let indexOfMutedUser = user.mutedUsers.indexOf(mutedUserId);

        if (indexOfMutedUser === -1) {
          user.mutedUsers.push(mutedUserId);
        }

        else {
          user.mutedUsers.splice(indexOfMutedUser, 1);
        }

        return user.save();

      }).then(function(updatedUser) {
        resolve(updatedUser);
      }).catch(function(e) {
        console.error(e);
        reject(e);
      });

    })

  }

};