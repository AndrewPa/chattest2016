'use strict';

let express = require('express');
let cookieParser = require('cookie-parser');
let path = require('path');
let bodyParser = require('body-parser');
var exphbs  = require('express-handlebars');

let routeFunctions = require('./routeFunctions');
let passport = require('./setUpPassport').default;
let sessionMiddleware = require('./sessionMiddleware').default;

let app = express();

app.use(express.static('../client/build/deps'));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('views', path.resolve(__dirname + '/../../../client/build'));
app.set('view engine', 'hbs');

app.use(cookieParser());
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());

// Set up routers
app.get('/', routeFunctions.indexRoute);
app.get('/main', routeFunctions.mainRoute);
app.get('/signout', routeFunctions.signOutRoute);
app.get('/disconnected', routeFunctions.disconnectedRoute);

app.get('/signin', routeFunctions.signInRoute);
app.get('/signup', routeFunctions.signUpRoute);

app.post('/signin', routeFunctions.signInPost);
app.post('/signup', routeFunctions.signUpPost);

app.get('*', routeFunctions.doesNotExist);

exports.default = app;