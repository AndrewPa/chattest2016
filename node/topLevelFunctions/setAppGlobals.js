'use strict';

let events = require('events');

exports.default = function() {

  /*
   *global.chattestServer*
   A globally-available namespace to be used in extremely limited cases, for example to
   store the global.channel, and constant values to avoid the use of magic numbers/strings.
   */
  global.chattestServer = {};

  let socketsForUsers = new Map;

  global.chattestServer.setSocketForUser = function(user, socket) {
    socketsForUsers.set(user, socket);
  };

  global.chattestServer.getSocketForUser = function(user) {
    return socketsForUsers.get(user);
  };

  /* TODO: Encapsulate this API elsewhere */
  global.chattestServer.channels = new Map;

  global.chattestServer.createChannelForSocketId = function(socketInstanceId) {
    global.chattestServer.channels.set(socketInstanceId, new events.EventEmitter);
  };

  global.chattestServer.getChannelForSocketId = function(socketInstanceId) {
    return global.chattestServer.channels.get(socketInstanceId);
  };

  global.chattestServer.destroyChannelForSocketId = function(socketInstanceId) {
    global.chattestServer.channels.delete(socketInstanceId);
  };

  // All constants to be used throughout the app
  global.chattestServer.constants = {
    listeningPort: 80,
    msgGetLimit: 30
  };

};