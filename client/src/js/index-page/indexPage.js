import initIndexMain from './deps/indexMain';
import initIndexSignUp from './deps/indexSignup';

let $ = require('jquery');

$(document).ready(function() {

  initIndexMain();
  initIndexSignUp();

});