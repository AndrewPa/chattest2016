let $ = require('jquery');
let errorContainer;

$(document).ready(function() {
  errorContainer = $("#error-container");
});

export default {

  displayError: function displayError(errorMsg) {
    errorContainer.text(errorMsg);
  },

  clearErrors: function clearErrors() {
    errorContainer.html("");
  }

}