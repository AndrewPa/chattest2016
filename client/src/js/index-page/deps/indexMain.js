import ErrorUtils from './ErrorUtils';

let $ = require('jquery');

export default function() {

  let pageContainer = $("#index-page-container");

  let mainPage = $("#main-page");
  let signUpPage = $("#signup-page");

  let loginNameInput = $("#login-input-username");
  let loginPassInput = $("#login-input-password");

  let loginButton = $("#login-button");
  let signUpPageLink = $("#signup-page-link");

  signUpPageLink.click(function () {

    pageContainer.addClass("hiding");
    pageContainer.on("transitionend", function () {

      ErrorUtils.clearErrors();

      mainPage.addClass("hidden");
      pageContainer.removeClass("hiding");
      signUpPage.removeClass("hidden");

    });
  });

  let loginSuccess = function(res) {
    window.location.href = "/main";
  };

  let loginFailure = function(res) {
    let errorMsg = JSON.parse(res.responseText).error;
    ErrorUtils.displayError(errorMsg);
  };

  loginButton.click(function() {

    let username = loginNameInput.val();
    let password = loginPassInput.val();

    if (username.length < 1 ||
        password.length < 1) {
      ErrorUtils.displayError(`You need to type in a username and password!`);
      return false;
    }

    $.ajax({
      type: "POST",
      url: "/signin",
      data: {
        "username": username,
        "password": password
      },
      success: loginSuccess,
      error: loginFailure
    });

    return true;

  });

}