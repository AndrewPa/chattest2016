export default {

  hasExtraWhitespace: function hasExtraWhitespace(value) {
    return value.trim() !== value;
  }

}