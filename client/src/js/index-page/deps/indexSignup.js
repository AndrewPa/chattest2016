import BasicValidation from './BasicValidation';
import ErrorUtils from './ErrorUtils';

let $ = require('jquery');

export default function() {

  let pageContainer = $("#index-page-container");

  let mainPage = $("#main-page");
  let signUpPage = $("#signup-page");

  let signUpNameInput = $("#signup-input-username");
  let signUpPassInput = $("#signup-input-password");
  let signUpPassInputRepeat = $("#signup-input-password-repeat");

  let signUpButton = $("#signup-button");
  let mainPageLink = $("#main-page-link");

  mainPageLink.click(function() {

    pageContainer.addClass("hiding");
    pageContainer.on("transitionend", function() {

      signUpPage.addClass("hidden");
      pageContainer.removeClass("hiding");
      mainPage.removeClass("hidden");

    });
  });

  let signUpSuccess = function(res) {
    window.location.href = "/main";
  };

  let signUpFailure = function(res) {
    let errorMsg = JSON.parse(res.responseText).error;
    ErrorUtils.displayError(errorMsg);
  };

  signUpButton.click(function() {

    let username = signUpNameInput.val();
    let password = signUpPassInput.val();
    let passwordRepeat = signUpPassInputRepeat.val();

    if (BasicValidation.hasExtraWhitespace(username)) {
      ErrorUtils.displayError(`Please remove the extra space around your username.`);
      return false;
    }

    if (username.length < 1 ||
      password.length < 1) {
      ErrorUtils.displayError(`You need to type in a username and password!`);
      return false;
    }

    if (password.repeat.length < 1) {
      ErrorUtils.displayError(`You need to repeat your password.`);
      return false;
    }
    
    $.ajax({
      type: "POST",
      url: "/signup",
      data: {
        "username": username,
        "password": password,
        "password_repeat": passwordRepeat
      },
      success: signUpSuccess,
      error: signUpFailure
    });

  });

}