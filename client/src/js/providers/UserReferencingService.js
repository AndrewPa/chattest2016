export default class UserReferencingService {

  constructor(UserDataService, UserMutingService) {

    let self = this;

    let userId;
    UserDataService.getUserDataPromise().then(function(userData) {
      userId = userData.userId;
    });

    self.parseReferences = function(body) {
      return body.replace(`@${userId}`, `<span class="referred-to-user">@${userId}</span>`);
    };

    self.referToUser = function(username) {

      let messageInput = chattestApp.dom.messageInput;

      if ((UserMutingService.checkIfUserMuted(username)) ||
          (userId === username)) { return; }

      let origSendMsgVal = messageInput.val();
      if (!origSendMsgVal.match(`@${username}`)) {
        messageInput.val(`@${username} ${origSendMsgVal}`);
      }

      setTimeout(function() {messageInput[0].focus();}, 0);

    };

  }

}