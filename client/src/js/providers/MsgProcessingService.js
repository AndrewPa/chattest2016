import constants from '../constants';
import parseLinks from './MsgProcessingDeps/LinkParsing';
import { getTimeAgo, getDisplayDate } from './MsgProcessingDeps/MessageDateTimes'

export default class MsgProcessingService {

  constructor(UserMutingService, UserReferencingService) {

    let processors = [
      parseLinks,
      UserReferencingService.parseReferences
    ];

    let processMessageBody = function(body) {

      processors.forEach(function(processor) {
        body = processor(body);
      });

      return body;

    };

    this.updateTimeAgo = function(msgObjs) {
      msgObjs.forEach(function(msgObj) {
        msgObj.timeAgo = getTimeAgo(msgObj.date);
      });
    };
    
    // Using fat-arrow notation to enforce lexical "this" instead
    // of the var self = this pattern
    let processMessageArray = (msgObjs) => {

      msgObjs = UserMutingService.filterOutMutedUserMessages(msgObjs);

      msgObjs.forEach(function(msgObj) {
        msgObj.body = processMessageBody(msgObj.body);
        msgObj.displayDate = getDisplayDate(msgObj.date);
      });

      this.updateTimeAgo(msgObjs);

      return msgObjs;

    };

    let combineAndOrderMessages = function(currentMsgs, newMessages) {
      return newMessages.concat(currentMsgs).slice(0, constants.SHOWN_MESSAGES_COUNT);
    };

    this.processLatestMessages = function(currentMsgs, receivedMsgs) {

      let messages;

      if (!Array.isArray(receivedMsgs)) {
        messages = [receivedMsgs];
      }
      else {
        messages = receivedMsgs;
      }

      let processedMessageArray = processMessageArray(messages);
      return combineAndOrderMessages(currentMsgs, processedMessageArray);

    };

  }

}