import EventEmitter from 'wolfy87-eventemitter';

export default class EventChannelService {

  constructor() {
    this._channel = new EventEmitter();
  }

  getChannel() {
    return this._channel;
  }

}