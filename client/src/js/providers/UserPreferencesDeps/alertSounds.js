// Expected filename style: name.format

// Sounds courtesy of soundbible.com
// Authors: Mark DiAngelo, DeepFrozenApps, Mike Koenig, snottyboy
let allSoundFilenames = [
  "tick.mp3",
  "bop.mp3",
  "pop.mp3",
  "ping.mp3"
];

let alertSounds = [];

allSoundFilenames.forEach(function (filename) {
  let audioName = filename.split(".")[0];
  let audioObj = new Audio("sounds/" + filename);
  alertSounds.push({
    name: audioName,
    sound: audioObj
  });
});

export default alertSounds;