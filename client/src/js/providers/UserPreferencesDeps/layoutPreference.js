import Preference from './Preference';

// TODO: More layouts are a project for future versions, just default for now
let layoutNames = [
  "default"
];

let layoutPreference = new Preference(layoutNames);

layoutPreference.changePreference = function changePreference() {
  //To be implemented after additional layouts are made
};

export default layoutPreference;