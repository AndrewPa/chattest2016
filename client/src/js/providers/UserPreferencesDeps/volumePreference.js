import Preference from './Preference';
import alertSounds from './alertSounds';

let volumeLevelNames = [
  "full",
  "half",
  "mute"
];

let volumeDefinitions = {
  "full": 10,
  "half": 5,
  "mute": 0
};

let volumePreference = new Preference(volumeLevelNames);

volumePreference.changePreference = function changePreference() {

  let newVolumeLevelName = this.preferenceItems[this._preferenceIndex];
  let newVolumeLevel = volumeDefinitions[newVolumeLevelName];

  alertSounds.forEach(function(soundObj) {
    soundObj.sound.volume = newVolumeLevel/volumeDefinitions.full;
  });

};

export default volumePreference;