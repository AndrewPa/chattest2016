import Preference from './Preference';
import alertSounds from './alertSounds';

let soundAlertPreference = new Preference(alertSounds);

soundAlertPreference.getCurrentPrefName = function() {
  return this.preferenceItems[this._preferenceIndex].name;
};

let playMsgReceivedSoundUnbound = function() {
  alertSounds[this._preferenceIndex].sound.play();
};

let playMsgReceivedSound = playMsgReceivedSoundUnbound.bind(soundAlertPreference);

export { soundAlertPreference as default, playMsgReceivedSound }