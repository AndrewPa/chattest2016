let Preference = function(preferenceItems) {
  this._preferenceIndex = 0;
  this.preferenceItems = preferenceItems;
  this.preferenceCount = preferenceItems.length;
};

Preference.prototype = {

  // "Abstract" method
  changePreference: function() {},

  cycleThroughPreference: function cycleThroughPreference(direction) {

    switch (direction) {
      case 'L':
        if (this._preferenceIndex === 0) {
          this._preferenceIndex = (this.preferenceCount - 1);
        }
        else {
          this._preferenceIndex--;
        }
        break;
      case 'R':
        this._preferenceIndex++;
        break;
      default:
        console.error(`Preference indices can only be cycled left (L) or right (R).`)
    }

    this._preferenceIndex = (this._preferenceIndex % this.preferenceCount);
    this.changePreference();

  },

  getCurrentPrefIndex: function getCurrentPrefIndex() {
    return this._preferenceIndex;
  },

  getCurrentPrefName: function getCurrentPrefName() {
    return this.preferenceItems[this._preferenceIndex];
  },

  setAndChangePref: function setPrefIndex(newValue) {
    this._preferenceIndex = newValue;
    this.changePreference();
  }

};

export default Preference;