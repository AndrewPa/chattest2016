import Preference from './Preference';

let colorSchemeNames = [
  "blueberry",
  "mango",
  "grape",
  "orange",
  "strawberry",
  "lime"
];

let schemeRegExp = new RegExp('scheme-.*');

let colorSchemePreference = new Preference(colorSchemeNames);

colorSchemePreference.getCurrentPrefName = function() {
  return this.preferenceItems[this._preferenceIndex];
};

colorSchemePreference.changePreference = function changePreference() {

  let documentBody = chattestApp.dom.documentBody;
  let documentBodyClassList = documentBody.attr("class").split(/\s+/);

  let curColScheme = documentBodyClassList.find(function(cssClass) {
    return cssClass.match(schemeRegExp);
  });

  let newColScheme = `scheme-${this.preferenceItems[this._preferenceIndex]}`;

  documentBody.removeClass(curColScheme);
  documentBody.addClass(newColScheme);

};

export default colorSchemePreference;