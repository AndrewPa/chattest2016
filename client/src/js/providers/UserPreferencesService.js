import colorSchemePreference from './UserPreferencesDeps/colorSchemePreference';
import layoutPreference from './UserPreferencesDeps/layoutPreference';
import soundAlertPreference from './UserPreferencesDeps/soundAlertPreference';
import volumePreference from './UserPreferencesDeps/volumePreference';

export default class UserPreferencesService {

  constructor(SocketService, UserDataService) {

    let prefOptions = {
      color: {
        fullName: "Color Scheme",
        prefObj: colorSchemePreference
      },
      layout: {
        fullName: "Room Layout",
        prefObj: layoutPreference
      },
      sound: {
        fullName: "Message Alert Sound",
        prefObj: soundAlertPreference
      },
      volume: {
        fullName: "Volume Level",
        prefObj: volumePreference
      }
    };

    let getUserPrefs = function() {
      let prefArr = [];
      Object.keys(prefOptions).forEach(function(prefName) {
        let prefData = {
          preference: prefName,
          value: prefOptions[prefName].prefObj.getCurrentPrefIndex()
        };
        prefArr.push(prefData);
      });
      return prefArr;
    };

    let setUserPrefs = function(userPrefsArr) {
      userPrefsArr.forEach(function(pref) {
        let prefObj = prefOptions[pref.preference].prefObj;
        prefObj.setAndChangePref(pref.value);
      });
    };

    UserDataService.getUserDataPromise().then(function(userData) {
      setUserPrefs(userData.preferences);
    });

    this.savePreferences = function() {
      let clientPrefs = getUserPrefs();
      SocketService.requestSavePreferences(clientPrefs);
    };

    this.getPreferenceOptions = function() {
      return prefOptions;
    };

  }

};