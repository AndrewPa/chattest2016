let socketReceiverConfig = [
  {
    receivedEventName: "userAlreadyHasConnection",
    handlerEventName: "alertUserForcedDisconnect"
  },
  {
    receivedEventName: "newChatData",
    handlerEventName: "gotNewChatData"
  },
  {
    receivedEventName: "newOnlineUserList",
    handlerEventName: "clientGotOnlineUsers"
  },
  {
    receivedEventName: "newUserData",
    handlerEventName: "clientGotUserData"
  },
  {
    receivedEventName: "userMuteToggled",
    handlerEventName: "clientUserMuteToggled"
  }
];

export default function setSocketReceivers(handlerEventChannel, socket) {

  socketReceiverConfig.forEach(function(receiverConfig) {

    let receivedEventName = receiverConfig.receivedEventName;
    let handlerEventName = receiverConfig.handlerEventName;
    
    socket.on(receivedEventName, function(...args) {
      handlerEventChannel.emit(handlerEventName, ...args);
    });

  });

}