let socketEmitterConfig = [
  {
    methodName: "pullUserData",
    eventName: "clientGetUserData"
  },
  {
    methodName: "pullOnlineUserList",
    eventName: "clientGetOnlineUsers"
  },
  {
    methodName: "sendMsg",
    eventName: "chatMsg"
  },
  {
    methodName: "requestSavePreferences",
    eventName: "savePreferences"
  },
  {
    methodName: "requestToggleMutedUser",
    eventName: "toggleMutedUser"
  },
  {
    methodName: "pullMsgsFromServer",
    eventName: "clientGetNewMsgs"
  }
];

export default function setSocketEmitters(socket) {

  let self = this;

  socketEmitterConfig.forEach(function (emitterConfig) {

    let methodName = emitterConfig.methodName;
    let eventName = emitterConfig.eventName;

    self[methodName] = function (...args) {
      socket.emit(eventName, ...args);
    };

  });

}