let moment = require('moment');
let localTzOffset = (new Date).getTimezoneOffset();

export function getTimeAgo(messageDate) {
  let serverTimeUtc = moment(messageDate).utc();
  return moment(serverTimeUtc).from(moment.max(serverTimeUtc, moment().utc()));
}

export function getDisplayDate(messageDate) {
  return moment(messageDate).subtract(localTzOffset, 'minutes').format('YYYY-MM-DD HH:MM:SS');
}