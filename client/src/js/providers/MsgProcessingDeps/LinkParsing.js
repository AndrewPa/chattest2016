import he from 'he';

let self = this;

let re_cpts = {
  http_unq: "https?:\\/\\/(?:\\w{1,20}\\.)?",
  www_unq: "www\\.",
  common: "[\\w-]{1,70}\\.\\w{1,20}(?:\\.\\w{1,20})?" +
  "(?:\\/(?:\\w*\\/*)?.*?(?:\\s|$|\\W$|\\W\\s|[.,;:!?]{2}|\\?{2}))?"
};

let linkRegexp = new RegExp(
  re_cpts.http_unq + re_cpts.common + "|" + re_cpts.www_unq + re_cpts.common
);

let addHrefAndTags = function(urlInBody) {

  let linkIndicator = `[link]`;

  var url = urlInBody.trim();
  var url_no_suf = url.substr(0, url.length - 2);
  var suffix = url.substr(url.length - 2, url.length);
  var suffix_rem = "";
  var suffix_cor = "";

  if (suffix.match(/[^A-Za-z0-9_\/]\W/)) {
    suffix_cor = "";
    suffix_rem = suffix;
  }
  else if (suffix.match(/\w[^A-Za-z0-9_\/]|\/\W/)) {
    suffix_cor = suffix.split("")[0];
    suffix_rem = suffix.split("")[1];
  }
  else if (suffix.match(/\w[\w\/]|\W\w/)) {
    suffix_cor = suffix;
    suffix_rem = "";
  }

  if (!url.match(/https?:\/\//)) {
    var prefix = "//";
  }
  else {
    prefix = "";
  }

  return `
    <a href="${prefix}${url_no_suf}${suffix_cor}" target="_blank"><em>${linkIndicator}</em></a>${suffix_rem}
  `;

};

export default function parseLinks(msgBody) {

  let remainingBody = he.decode(msgBody);
  let processedBody = "";

  let maxIter = 100;
  let iterCount = 0;

  while (true) {

    let curMatch = remainingBody.match(linkRegexp);

    if (!curMatch) {
      processedBody += he.encode(remainingBody);
      return processedBody;
    }

    let curMatchIdx = curMatch.index;
    let curMatchValue = curMatch[0];

    let sanitizedContentBeforeUrl = he.encode(remainingBody.slice(0, curMatchIdx));
    let sanitizedUrlMatch = he.encode(curMatchValue);

    processedBody += sanitizedContentBeforeUrl;
    processedBody += addHrefAndTags(sanitizedUrlMatch);

    remainingBody = remainingBody.slice(curMatchIdx + curMatchValue.length);

    if (++iterCount > maxIter) {
      console.error(`There was a problem parsing links in a message body, please review LinkParsingService.`);
      return msgBody;
    }

  }

};