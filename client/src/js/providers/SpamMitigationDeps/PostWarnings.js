let WarningDisplayFactory = function(warning, duration) {

  return function() {

    let messageInput = chattestApp.dom.messageInput;

    messageInput.val("");
    messageInput.placeholder = warning;
    setTimeout(function () {
      messageInput.placeholder = "";
    }, duration);

    return false;

  }

};

let lengthWarning = WarningDisplayFactory("The message you tried to send was too long.", 3000);
let frequencyWarning = WarningDisplayFactory("Wait 1 second between posts.", 1000);
let wordLengthWarning = WarningDisplayFactory("Please do not flood the room.", 3000);

export { lengthWarning, frequencyWarning, wordLengthWarning }