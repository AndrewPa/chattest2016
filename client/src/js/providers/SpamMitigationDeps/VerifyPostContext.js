import constants from '../../constants';
import postCooldown from './PostCooldown';

let re_cpts = {
  http_unq: "https?:\\/\\/(?:\\w{1,20}\\.)?",
  www_unq: "www\\.",
  common: "[\\w-]{1,70}\\.\\w{1,20}(?:\\.\\w{1,20})?" +
  "(?:\\/(?:\\w*\\/*)?.*?(?:\\s|$|\\W$|\\W\\s|[.,;:!?]{2}|\\?{2}))?"
};

let linkRegexp = new RegExp(
  re_cpts['http_unq'] + re_cpts['common'] + "|" + re_cpts['www_unq'] + re_cpts['common'],
  ["g"]
);

let verifyLength = function verifyLength() {

  let messageInputValue = chattestApp.dom.messageInput.val();

  if(messageInputValue.length > constants.MAXIMUM_MESSAGE_LENGTH) {
    postWarnings.lengthWarning();
    return false;
  }
  else if (messageInputValue.length < constants.MINIMUM_MESSAGE_LENGTH) {
    return false;
  }

  return true;

};

let verifyFrequency = function verifyFrequency(post_cooldown) {
  // TODO: Message cooldown?
  if(false) {
    this._PostWarningService.frequencyWarning();
    return false;
  }
  else {
    return true;
  }
};

let verifyWordLength = function verifyWordLength() {
  var user_msg = chattestApp.dom.messageInput.val();
  var user_msg_nolink = user_msg.replace(linkRegexp, "");
  var msg_arr = user_msg_nolink.trim().split(/\s/);

  for (var i=0;i<msg_arr.length;i++) {
    if (msg_arr[i].length > 50) {
      postWarnings.wordLengthWarning();
      return false;
    }
  }
  return true;
};

export { verifyLength, verifyFrequency, verifyWordLength }