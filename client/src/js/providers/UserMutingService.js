export default class UserMutingService {

  constructor(UserDataService, SocketService) {

    var self = this;
    let clientUserId;

    let setMutedUsers = function(mutedUsersArr) {
      mutedUsersArr.forEach(function(mutedUser) {
        self.toggleMuteUserLocal(mutedUser);
      });
    };

    UserDataService.getUserDataPromise().then(function(userData) {
      clientUserId = userData.userId;
      setMutedUsers(userData.mutedUsers);
    });

    let mutedUsers = {};

    this.toggleMuteUserLocal = function(mutedUserId) {

      if (typeof mutedUsers[mutedUserId] === "undefined") {
        mutedUsers[mutedUserId] = true;
      }
      else {
        delete mutedUsers[mutedUserId];
      }

    };

    this.toggleMuteUser = function(mutedUserId) {
      if (clientUserId !== mutedUserId) {
        SocketService.requestToggleMutedUser(mutedUserId);
        return true;
      }
      return false;
    };

    this.checkIfUserMuted = function(targetUserId) {
      return mutedUsers[targetUserId];
    };

    this.filterOutMutedUserMessages = function(msgObjs) {
      return msgObjs.filter(function(msgObj) {
        return !mutedUsers[msgObj.sentBy];
      });
    };

  }

}