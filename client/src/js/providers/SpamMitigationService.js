import { lengthWarning, frequencyWarning, wordLengthWarning } from './SpamMitigationDeps/PostWarnings';
import { verifyLength, verifyFrequency, verifyWordLength } from './SpamMitigationDeps/VerifyPostContext';

export default class SpamMitigationService {

  constructor() {

    let self = this;

    function isMessageEmpty(msg) {
      return chattestApp.dom.messageInput.val().trim().length === 0;
    }

    function isSendingBehaviorSpammy() {

      return  !(
        verifyLength() &&
        verifyFrequency() &&
        verifyWordLength()
      );

    }

    this.isPossibleSpam = function(msg) {

      return (
        isMessageEmpty(msg) ||
        isSendingBehaviorSpammy()
      );

    };

  }

}