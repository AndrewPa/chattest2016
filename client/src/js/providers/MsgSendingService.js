import constants from '../constants';

export default class MsgSendingService {

  constructor(SpamMitigationService, SocketService) {

    let self = this;

    self.sendMsg = function(e, c) {

      if((e.which !== 13 && c !== "click")) { return; }

      let messageInput = chattestApp.dom.messageInput;
      let messageArea = chattestApp.dom.messageArea;

      let toSend = chattestApp.dom.messageInput.val();

      if (!SpamMitigationService.isPossibleSpam(toSend)) {

        SocketService.sendMsg({body: toSend});
        messageArea.scrollTop = 0;

        if (document.body.clientHeight <= constants.HEIGHT_BLUR_TOGGLE_CUTOFF) {
          setTimeout(function () { messageInput[0].blur(); }, 0);
        }

      }

      messageInput.val("");

    };

  }

}