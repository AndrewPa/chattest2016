import io from 'socket.io-client';

import setSocketEmitters from './SocketServiceDeps/SocketEmitters';
import setSocketReceivers from './SocketServiceDeps/SocketReceivers';

export default class SocketService {

  constructor(EventChannelService) {

    let self = this;
    let clientSocket = io();
    let chattestChannel = EventChannelService.getChannel();

    // Establish socket emitter API on SocketService
    setSocketEmitters.call(self, clientSocket);
    // Associate socket received events to handler events on client event channel
    setSocketReceivers(chattestChannel, clientSocket);

    self.getSocket = function() {
      return clientSocket;
    }

  }

}