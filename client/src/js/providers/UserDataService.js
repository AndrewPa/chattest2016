export default class UserDataService {

  constructor(SocketService, EventChannelService) {

    let self = this;
    let curUserId;

    SocketService.pullUserData();
    let chattestChannel = EventChannelService.getChannel();

    let userDataPromise = new Promise(function(resolve, reject) {
      chattestChannel.on('clientGotUserData', function(userData) {
        curUserId = userData.userId;
        resolve(userData);
      });
    }).catch(function(e) {
      console.error(e);
    });

    self.getUserDataPromise = function() {
      return userDataPromise;
    };

    self.isCurrentUser = function(username) {
      return username === curUserId;
    };

  }

}