/* Copyright 2016 AndrewPa */

// Since the sources are concatenated, strict mode is enforced on
// all sources that have app.js as a dependency entry point
'use strict';

import 'lodash';
import angular from 'angular';
import angularSanitize from 'angular-sanitize';

import mainController from './controllers/mainController';

import EventChannelService from './providers/EventChannelService';
import SocketService from './providers/SocketService';

import UserPreferencesService from './providers/UserPreferencesService';
import SpamMitigationService from './providers/SpamMitigationService';
import MsgProcessingService from './providers/MsgProcessingService';
import UserReferencingService from './providers/UserReferencingService';
import UserDataService from './providers/UserDataService';
import UserMutingService from './providers/UserMutingService';
import MsgSendingService from './providers/MsgSendingService';

import chatMessageArea from './components/chatMessageArea';
import preferenceOption from './components/preferenceOption';

// Init main Angular module, use as namespace
window.chattestApp = angular.module('chattestApp', [angularSanitize])

  .service('SocketService', ['EventChannelService', SocketService])
  .service('EventChannelService', [EventChannelService])
  .service('UserDataService', ['SocketService', 'EventChannelService', UserDataService])
  .service('UserMutingService', ['UserDataService', 'SocketService', UserMutingService])
  .service('MsgSendingService', ['SpamMitigationService', 'SocketService', MsgSendingService])
  .service('UserPreferencesService', ['SocketService', 'UserDataService', UserPreferencesService])
  .service('MsgProcessingService', ['UserMutingService', 'UserReferencingService', MsgProcessingService])
  .service('UserReferencingService', ['UserDataService', 'UserMutingService', UserReferencingService])
  .service('SpamMitigationService', [SpamMitigationService])


  .directive('chatMessageArea', [chatMessageArea])
  .component('preferenceOption', preferenceOption)

  .controller('mainController', ['$scope', '$interval', 'MsgProcessingService',
              'SocketService', 'EventChannelService',
              'UserPreferencesService', 'UserMutingService', 'UserDataService',
              'UserReferencingService', 'MsgSendingService', mainController]);

document.addEventListener("DOMContentLoaded", function() {

  let $ = angular.element;

  // Namespace for all DOM queries, convention is to use jqLite for all of them
  chattestApp.dom = {

    documentBody: $(document.body),

    inputToolbar: $(document.getElementById("input-toolbar")),
    messageInput: $(document.getElementById("message-input")),

    messageArea: $(document.getElementById("message-area")),
    chatMessage: $(document.getElementById("chat-msg"))

  };

});