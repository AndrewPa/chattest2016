import constants from '../constants';
import { playMsgReceivedSound } from '../providers/UserPreferencesDeps/soundAlertPreference';

export default class mainController {

  constructor($scope, $interval, MsgProcessingService,
              SocketService, EventChannelService,
              UserPreferencesService, UserMutingService, UserDataService,
              UserReferencingService, MsgSendingService) {

    var main = $scope.mainCtrl = this;
    main.messages = [];

    (function runAdditionalControllerInitFunctions() {

      SocketService.pullMsgsFromServer();

      $interval(function() {
        MsgProcessingService.updateTimeAgo(main.messages);
      }, constants.MOMENT_TIME_DIFF_INTERVAL);

    })();

    (function exposeValuesAndMethodsToController() {

      /* Service Methods */
      main.sendMsg = MsgSendingService.sendMsg;
      main.isUserMuted = UserMutingService.checkIfUserMuted;
      main.savePreferences = UserPreferencesService.savePreferences;
      main.isCurrentUser = UserDataService.isCurrentUser;

      /* Service States */
      main.preferenceOptions = UserPreferencesService.getPreferenceOptions();

      /* Simple Display Methods & Display States */
      main.isModalDialogShown = false;
      main.togglePreferences = function() { main.isModalDialogShown = !main.isModalDialogShown; };

      main.logoutConfirm = function() {
        //logout_dialog.dialog("open");
      };

      /* Dynamic API Exposure */
      main.deactivateUserMuter = function() {
        main.userMuter = false;
        main.handleUsernameClick = UserReferencingService.referToUser;
      };

      main.handleUsernameClick = UserReferencingService.referToUser;
      main.activateUserMuter = function() {
        main.userMuter = true;
        main.handleUsernameClick = function(username) {
          let userSuccessfullyMuted = UserMutingService.toggleMuteUser(username);
          if (userSuccessfullyMuted) { main.deactivateUserMuter(); }
        }
      };

    })();

    EventChannelService.getChannel().on('gotNewChatData', function(newMessageData) {
      if (newMessageData.length === 0) { return; } // All user messages were filtered out from muting
      main.messages = MsgProcessingService.processLatestMessages(main.messages, newMessageData);
      $scope.$apply();
      playMsgReceivedSound();
    });

    SocketService.pullOnlineUserList();
    EventChannelService.getChannel().on('clientGotOnlineUsers', function(onlineUsersList) {
      main.onlineUsers = onlineUsersList;
      $scope.$apply();
    });

    EventChannelService.getChannel().on('clientUserMuteToggled', function(mutedUserId) {
      UserMutingService.toggleMuteUserLocal(mutedUserId);
      $scope.$apply();
    });

    EventChannelService.getChannel().on('alertUserForcedDisconnect', function() {
      window.location.href = '/disconnected';
    });

  };

}