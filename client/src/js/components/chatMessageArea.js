import Constants from '../constants';

export default function ChatMessageArea() {

  var template = '';

  for (var i = 0; i < Constants.SHOWN_MESSAGES_COUNT; i++) {
    template += `
      <div class="user-message" ng-show="main.messages[${i}]">
        <span class="user-message-name" ng-click="main.referToUser()">{{main.messages[${i}].sentBy}}:</span>
        <span class="user-message-msg" ng-bind-html="main.messages[${i}].body"></span>
        <span class="user-message-dt">{{main.messages[${i}].timeAgo}}</span>
      </div>
    `}

  return {
    restrict: 'E',
    template: template
  };

}