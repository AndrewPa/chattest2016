export default {

  controllerAs: "pOption",
  bindings: {
    pref: "<"
  },
  controller: function() {

    let pOption = this;
    pOption.pObj = pOption.pref.prefObj;

  },
  templateUrl: './templates/preferenceOption.html'

};