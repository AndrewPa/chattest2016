'use strict';

var gulp = require('gulp');
var watchify = require('watchify');
var babelify = require('babelify');
var browserify = require('browserify');
var vinylSourceStream = require('vinyl-source-stream');
var vinylBuffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var path = require('path');
var sass = require('gulp-sass');
var merge = require('utils-merge');
var rename = require('gulp-rename');
var htmlmin = require('gulp-htmlmin');
var mustache = require("gulp-mustache");

var src = {
  compTemplates: 'src/templates/*.html',
  pageTemplates: ['src/*.hbs', 'src/*.html'],
  libs: 'src/libs/**',
  js: {
    all: 'src/js/**/*.js',
    app: 'src/js/app.js'
  },
  styles: {
    scss: 'src/scss/*.scss'
  },
  indexJs: {
    file: 'src/js/index-page/indexPage.js'
  },
  mustache: {
    all: 'src/site-pages/page-scaffolds/*.mustache'
  }
};

var buildDir = 'build/deps';
var out = {
  pageTemplates: 'build',
  compTemplates: buildDir + '/templates',
  js: {
    file: 'app.min.js',
    fileNoMin: 'app.js',
    folder: buildDir + '/js/'
  },
  indexJs: {
    file: 'indexPage.min.js',
    fileNoMin: 'indexPage.js',
    folder: buildDir + '/js/'
  },
  css: {
    folder: buildDir + '/css'
  },
  mustache: {
    folder: 'build'
  }
};

gulp.task('mustache', function () {
  return gulp.src(src.mustache.all)
    .pipe(mustache())
    .pipe(rename({
      extname: ".hbs"
    }))
    .pipe(gulp.dest(out.mustache.folder));
});

var htmlminOpts = {
  removeComments: true,
  collapseWhitespace: true
};

gulp.task('sass', function () {
  return gulp.src(src.styles.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(out.css.folder));
});

gulp.task('sass:watch', function () {
  gulp.watch(src.styles.scss, ['sass']);
});

gulp.task('minify-page-templates', function() {
  return gulp.src(src.pageTemplates)
    .pipe(htmlmin(htmlminOpts))
    .pipe(gulp.dest(out.pageTemplates));
});

gulp.task('minify-page-templates:watch', function () {
  gulp.watch(src.pageTemplates, ['minify-page-templates']);
});

gulp.task('minify-component-templates', function() {
  return gulp.src(src.compTemplates)
    .pipe(htmlmin(htmlminOpts))
    .pipe(gulp.dest(out.compTemplates));
});

gulp.task('minify-component-templates:watch', function () {
  gulp.watch(src.compTemplates, ['minify-component-templates']);
});

var babelifyOpts = {};

gulp.task('index-js', function() {
  var bundler = browserify(src.indexJs.file).transform(babelify, babelifyOpts);
  return bundler.bundle()
    .pipe(vinylSourceStream(out.indexJs.fileNoMin))
    .pipe(vinylBuffer())
    .pipe(rename(out.indexJs.file))
    .pipe(uglify())
    .pipe(gulp.dest(out.indexJs.folder))
});

gulp.task('index-js:watch', function () {
  gulp.watch(src.indexJs.folder, ['indexJs']);
});

gulp.task('js-dev', function () {
  var bundler = browserify(src.js.app, { debug: true }).transform(babelify, babelifyOpts);
  return bundler.bundle()
    .pipe(vinylSourceStream(out.js.fileNoMin))
    .pipe(vinylBuffer())
    .pipe(gulp.dest(out.js.folder))
    .pipe(rename(out.js.file))
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(out.js.folder))
});

let bundleForWatchify = function(bundler) {
  return bundler.bundle()
    .pipe(vinylSourceStream(out.js.fileNoMin))
    .pipe(vinylBuffer())
    .pipe(gulp.dest(out.js.folder))
    .pipe(rename(out.js.file))
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(out.js.folder))
};

gulp.task('watchify', function () {
  var args = merge(watchify.args, { debug: true });
  var bundler = watchify(browserify(src.js.app, args)).transform(babelify, babelifyOpts);
  bundleForWatchify(bundler);

  bundler.on('update', function () {
    bundleForWatchify(bundler)
  })
});

gulp.task('browserify-prod', function () {
  var bundler = browserify(src.js.app).transform(babelify, babelifyOpts);
  return bundler.bundle()
    .pipe(vinylSourceStream(out.js.fileNoMin))
    .pipe(vinylBuffer())
    .pipe(rename(out.js.file))
    .pipe(uglify())
    .pipe(gulp.dest(out.js.folder))
});

gulp.task('default', ['watchify', 'sass:watch', 'minify-page-templates:watch',
  'minify-component-templates:watch', 'index-js:watch']);

gulp.task('deploy', ['browserify-prod', 'sass', 'minify-page-templates',
  'minify-component-templates', 'index-js', 'mustache']);